from django.shortcuts import render
from django.http import HttpResponse
import requests

# Create your views here.
def index(request):
    return render(request, 'story-8.html')

def getData(request):
    apikey = 'AIzaSyDvHEqdS5pvr3pIgLylXZP5AnZfYw7A4-Y'
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + request.POST['bookName'] + '&maxResults=40&key=' + apikey
    response = requests.get(url)
    if (response.status_code == 200) :
        return HttpResponse(response)