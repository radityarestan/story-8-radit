from django.test import TestCase, Client

# Create your tests here.
class TestStory8(TestCase):
    def test_if_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_if_content_is_exist(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')

        self.assertIn('Pencarian', html_response)
        self.assertIn('Buku', html_response)


    def test_ajax_request(self):
        response_ajax = Client().post('/getData/', {'bookName' : 'java'})
        self.assertEqual(response_ajax.status_code, 200)
        