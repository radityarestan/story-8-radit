$(document).ready(function() {
    var csrf = $('input[name=csrfmiddlewaretoken]').val();
    var totalResult;
    var counter;

    $(window).scroll(function () {
        if ($(window).scrollTop() >= $(document).height() - $(window).height()-50) {
            appendData();
        }
    });

    function appendData() {
        for (var i = 0; i < 10; i++) {
            var items = totalResult.items[counter];
            if (items != null) {
                $('table').append(
                    '<tr>' +
                    '<td class="result number">' + (counter+1) + '</td>' +
                    '<td class="result">' + items.volumeInfo.title + '</td>' +
                    '<td class="result"><img src="' + items.volumeInfo.imageLinks.thumbnail + '"></td>'+  
                    '<td class="result">' + items.volumeInfo.publisher + '</td>'+
                    '<td class="result"><a href="' + items.accessInfo.webReaderLink + '"><button type="button" class="btn">Lihat</button></a></td>'+
                    '</tr>'
                );
            }
            counter++;
        }
    }
    $('.search-button').click(function() {
        var bookName = $('.search-box').val();

        if (bookName.length == 0) {
            alert('Mohon diisi keywordnya dahulu');
        } else {
            counter = 0;
            $('.result-api').empty();  
            $(document).ajaxStart(function() {
                $('.loader').css('display', 'block');
            });
            $(document).ajaxComplete(function() {
                $('.loader').css('display', 'none');
            });
            $.ajax({
                url:'getData/',
                timeout: 3000,
                method: 'POST',
                data: {
                    csrfmiddlewaretoken: csrf,
                    bookName: bookName,
                },
                success: function(result) {
                    totalResult = jQuery.parseJSON(result);
                    console.log(totalResult);
                    $('.result-api').append(
                        '<table class="table table-hover table-dark"><thead><tr><th scope="col" class="result number">Nomor</th><th scope="col" class="result">Judul Buku</th>' +
                        '<th scope="col" class="result">Gambar</th><th scope="col" class="result">Publisher</th><th scope="col" class="result">Lihat Buku</th>' +
                        '</tr></thead></table>'
                    );
                    if (totalResult.totalItems != 0) {
                        appendData();
                    } else {
                        $('table').append('<tr><td colspan="5" class="result">Tidak ada keyword yang sesuai</td></tr>');
                    }
                },
                error: function() {
                    alert('Maaf telah terjadi error');
               },
            });
        }
    })
});

